'use strict';

/**
 * @ngdoc function
 * @name gliTp3AngluaJsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gliTp3AngluaJsApp
 */
angular.module('gliTp3AngluaJsApp')
  .controller('MainCtrl', function ($scope,$http,$location,$routeParams) {

    $scope.eltSelect={id : $routeParams.entity};
    $scope.elt = [
      {id :'language',name : 'Language', url:'views/partials/list/language.html'},
      {id :'userStorie',name : 'User Storie', url:'views/partials/list/userStorie.html'},
      {id :'user',name : 'User', url:'views/partials/list/user.html'},
      {id :'sprint',name : 'Sprint', url:'views/partials/list/sprint.html'},
      {id :'task',name : 'Task', url:'views/partials/list/task.html'}
    ];

    angular.forEach( $scope.elt,function(value){
      if (value.id === $routeParams.entity ){
        $scope.eltSelect=value;
      }
    });

    $scope.add = function(){
      $location.path('/add/'+$scope.eltSelect.id);
    };

    $scope.list = function(){
      $http.get('/'+$scope.eltSelect.id+'/all').then(function(data){
        $scope.result = data.data;
      });
    };

    $scope.info = function(id){
      $http.get('/'+$scope.eltSelect.id+'/'+id).then(function(data){
        $scope.result = data.data;

      });
    };

    $scope.remove = function(id){
      $http.delete('/'+$scope.eltSelect.id+'/'+id).then(function(){
        $scope. change();
      });
    };

    $scope.change = function(){
      $location.path('/'+$scope.eltSelect.id);
      if ($scope.eltSelect !== {}) $scope.list($scope.eltSelect.id)
    };

    if ($scope.eltSelect !== {}) $scope.list($scope.eltSelect.id)

  });
