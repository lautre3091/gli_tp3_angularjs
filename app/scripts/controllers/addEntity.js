angular.module('gliTp3AngluaJsApp')
  .controller('addEntityCtrl', function ($scope,$http,$location,$routeParams) {
    $scope.formAddTemplate = 'views/partials/add/'+$routeParams.entity+'.html';

    $scope.entity={};

    $scope.list = function(entity) {
      $http.get('/' + entity +'/all').then(function (data) {
        if (entity === 'sprint'){
          $scope.listSprints = data.data
        } else if (entity === 'language') {
          $scope.listLanguages = data.data;
        } else if (entity === 'task'){
          $scope.listTasks = data.data
        } else if (entity === 'user'){
          $scope.listUsers = data.data
        } else if (entity === 'userStorie'){
          $scope.listUserStories = data.data
        }
      });
    };

    $scope.addEntity=function(entity){
      var url = '/'+entity+'/create';
      var params = '';
      var send = false;
      var user = $scope.entity.user;
      if (entity === 'sprint'){
        url += '/'+$scope.entity.name;
        send = ($scope.entity.name != "");
      } else if (entity === 'language') {
        url += '/'+$scope.entity.name;
        send = ($scope.entity.name != null);
        if (user !== null && user !== undefined) url  += '/'+user.id;
      } else if (entity === 'task') {
        var userStorie = $scope.entity.userStorie;
        params = {description : $scope.entity.description,name : $scope.entity.name};
        if (userStorie !== null && userStorie !== undefined) url  += '/'+userStorie.id;
        send = ($scope.entity.description != "" && userStorie !== null && userStorie !== undefined  );
      } else if (entity === 'user') {
        params = {firstname : $scope.entity.firstname, lastname : $scope.entity.lastname};
        send = ($scope.entity.firstname !== null && $scope.entity.lastname !== null)
      } else if (entity === 'userStorie') {
        var sprint = $scope.entity.sprint;
        params = {description : $scope.entity.description,name : $scope.entity.name};
        if (sprint !== null && sprint !== undefined) url  += '/'+sprint.id;
        if (user !== null && user !== undefined) url  += '/'+user.id;
        send = ($scope.entity.description != "" && user !== null && user !== undefined && sprint !== null && sprint !== undefined);
      }
      if (send) $http.post(url, params).then(function () {
        $location.path('/'+entity);
      });
    };
  });
