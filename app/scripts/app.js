  'use strict';

/**
 * @ngdoc overview
 * @name gliTp3AngluarJsApp
 * @description
 * # gliTp3AngluaJsApp
 *
 * Main module of the application.
 */
angular
  .module('gliTp3AngluaJsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/:entity', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/add/:entity', {
        templateUrl: 'views/addEntity.html',
        controller: 'addEntityCtrl',
        controllerAs: 'addEntity'
      })
      .when('/info/:entity', {
        templateUrl: 'views/infoEntity.html',
        controller: 'infoEntityCtrl',
        controllerAs: 'infoEntity'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
