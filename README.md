GLI TP3 AngularJS 
===================
TP de mise en place d'une interface pour une application web avec **AngularJS**.

----------
[TOC]

----------

##1. Présentation

Ce projet est un site web qui permet de faire des appels à l'application web dévelloper dans le cours de TAA 

----------

##2. Fonctionnalités 

Le site offre plusieurs fonctionnalités qui sont les suivantes :
    - Lister, ajouter, supprimer toutes les entitées présente dans l'application écrite en TAA.

----------

##3. Utiliser le site web

Pour utiliser l'application il faut lancer la commande `grunt serve` à la racine du projet, cela va lancer le serveur pour utiliser le site web.

##4. Amélioration
Certaine fonctionnalitées n'ont pas été implementées. 
  - L'affichage d'étailler des entitées.
  - La modification des entitées.
  
  
